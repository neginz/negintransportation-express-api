/** 
 * Express Route: /cars
 * @author Clark Jeria
 * @version 0.0.3
 */
var express = require('express');
var router = express.Router();
var util = require('util');

var Car = require('../app/models/car');

var theResource = "car";
var theAttribute;
var theLength;
var theType;



router.route('/cars') 
    /**
     * GET call for the car entity (multiple).
     * @returns {object} A list of cars. (200 Status Code)
     * @throws Mongoose Database Error (500 Status Code)
     */
    .get(function(req, res){

        Car.find(function(err, cars){
            if (cars == "")
            {
                 res.status(404).json({"errorCode": "1009", "errorMessage" : util.format("No data exists for %s!", theResource), "statusCode" : "404"});
                 return;
            }
            if(err){
                res.status(500).send(err);
            }else{
                res.json(cars);
            }
        });
    })

    /**
     * POST call for the car entity.
     * @param {string} license - The license plate of the new car
     * @param {integer} doorCount - The amount of doors of the new car
     * @param {string} make - The make of the new car
     * @param {string} model - The model of the new car
     * @returns {object} A message and the car created. (201 Status Code)
     * @throws Mongoose Database Error (500 Status Code)
     */
    .post(function(req, res){

        var car = new Car();
        
        var x=0;
 
        for (var key in req.body){x++ }
        if (x==0)
        {
            res.status(400).json({"errorCode": "1001", "errorMessage" : util.format("Empty Request body for %s resource!", theResource), "statusCode" : "400"});
            return;
        }


       var json = req.body;
 
        for (var key in json) {
            if (key.trim() != "make" && key.trim() !="model" && key.trim() != "doorCount" && key.trim() != "license")
            {
                res.status(400).json({"errorCode": "1003", "errorMessage" : util.format("Invalid attribute '%s' for the '%s' resource!", key, theResource), "statusCode" : "400"});
                return;
            }

            if (json[key] == "")
            {
                res.status(400).json({"errorCode": "1003", "errorMessage" : util.format("Missing attribute value! Please provide a value for '%s' of the '%s' resource!", key, theResource), "statusCode" : "400"});
                return;
            }

            if (json[key] instanceof Array)
            {
                res.status(400).json({"errorCode": "1005", "errorMessage" : util.format("Duplicate attribute for '%s' of the '%s' resource!", key, theResource), "statusCode" : "400"});
                return;
            }       

        }

        var myKeys = ["make","model","license","doorCount"]
        for (i = 0; i < myKeys.length; i++) {
            if(!req.body.hasOwnProperty(myKeys[i]))
            {
              res.status(400).json({"errorCode": "1002", "errorMessage" : util.format("Missing attribute! Please provide '%s' of the '%s' resource!", myKeys[i], theResource), "statusCode" : "400"});
              return;
            }
        }


        theAttribute = "doorCount";
        if (isNaN(Number(req.body.doorCount)))
        {
                theType = "number"
                res.status(400).json({"errorCode": "1006", "errorMessage" : util.format("Invalid attribute type! Please provide a valid %s, for '%s' of the '%s' resource!", theType, theAttribute, theResource), "statusCode" : "400"});
                return;
        }

        if (Number(req.body.doorCount) < 1 || Number(req.body.doorCount) > 8)
        {
                var min = 1;
                var max = 8;
                res.status(400).json({"errorCode": "1014", "errorMessage" : util.format("Invalid range! '%s' should be between '%d' and '%d' for '%s' resource!", theAttribute,min,max, theResource), "statusCode" : "400"});
                return;
        }

        
        theAttribute = "make";
        if (req.body.make.trim().length > 18)
        {
                theLength = 18;
                res.status(400).json({"errorCode": "1013", "errorMessage" : util.format("Invalid length for '%s' of '%s' resource! Length for this attribute should be less than '%d' characters.", theAttribute,theResource,theLength), "statusCode" : "400"});
                return;
        }

        theAttribute = "model";
        if (req.body.model.trim().length > 18)
        {
                theLength = 18;
                res.status(400).json({"errorCode": "1013", "errorMessage" : util.format("Invalid length for '%s' of '%s' resource! Length for this attribute should be less than '%d' characters.", theAttribute,theResource,theLength), "statusCode" : "400"});
                return;
        }

        theAttribute = "license";
        if (req.body.license.trim().length > 18)
        {
                theLength = 18;
                res.status(400).json({"errorCode": "1013", "errorMessage" : util.format("Invalid length for '%s' of '%s' resource! Length for this attribute should be less than '%d' characters.", theAttribute,theResource,theLength), "statusCode" : "400"});
                return;
        }


        car.license = req.body.license;
        car.doorCount = req.body.doorCount;
        car.make = req.body.make;
        car.model = req.body.model;

        car.save(function(err){
            if(err){
                res.status(500).send(err);
            }else{
                res.status(201).json(car);
            }
        });
    });

/** 
 * Express Route: /cars/:car_id
 * @param {string} car_id - Id Hash of Car Object
 */
router.route('/cars/:car_id')
    /**
     * GET call for the car entity (single).
     * @returns {object} the car with Id car_id. (200 Status Code)
     * @throws Mongoose Database Error (500 Status Code)
     */
    .get(function(req, res){

        Car.findById(req.params.car_id, function(err, car){
            if(err){
                if (err.name == "CastError")    
                {
                    res.status(404).json({"errorCode": "1008", "errorMessage" : util.format("%s is an Invalid ID type for %s resource!", err.value,theResource), "statusCode" : "404"});
                    return;
                }
                res.status(500).send(err);
            }else{
                if (car == null)
                {
                    res.status(404).json({"errorCode": "1007", "errorMessage" : util.format(" No %s with ID='%s' exists!", theResource, req.params.car_id), "statusCode" : "404"});
                    return;
                }
                res.json(car);
            }
        });  
    })

    /**
     * PATCH call for the car entity (single).
     * @param {string} license - The license plate of the new car
     * @param {integer} doorCount - The amount of doors of the new car
     * @param {string} make - The make of the new car
     * @param {string} model - The model of the new car
     * @returns {object} A message and the car updated. (200 Status Code)
     * @throws Mongoose Database Error (500 Status Code)
     */
    .patch(function(req, res){

        var x=0;

        for (var key in req.body){x++ }
        if (x==0)
        {
            res.status(400).json({"errorCode": "1001", "errorMessage" : util.format("Empty Request body for %s resource!", theResource), "statusCode" : "400"});
            return;
        }

        Car.findById(req.params.car_id, function(err, car){
            if(err){
                if (err.name == "CastError")
                {
                    res.status(400).json({"errorCode": "1008", "errorMessage" : util.format("%s is an Invalid ID type for %s resource!", err.value,theResource), "statusCode" : "400"});
                    return;
                }
                res.status(500).send(err);
            }else{
                var json = req.body;
        
                for (var key in json) {
                    theAttribute = key;
                    if (key.trim() != "make" && key.trim() !="model" && key.trim() != "doorCount" && key.trim() != "license")
                    {
                        res.status(400).json({"errorCode": "1003", "errorMessage" : util.format("Invalid attribute '%s' for the '%s' resource!", key, theResource), "statusCode" : "400"});
                        return;
                    }

                    if (json[key].trim() == "")
                    {
                        res.status(400).json({"errorCode": "1004", "errorMessage" : util.format("Missing attribute value! Please provide a value for '%s' of the '%s' resource!", key, theResource), "statusCode" : "400"});
                        return;
                    }

                    if (json[key] instanceof Array)
                    {
                        res.status(400).json({"errorCode": "1005", "errorMessage" : util.format("Duplicate attribute for '%s' of the '%s' resource!", key, theResource), "statusCode" : "400"});
                        return;
                    }       

                    if (key.trim() == "doorCount")
                    {
                        if (isNaN(Number(req.body.doorCount)))
                        {
                                theType = "number";
                                res.status(400).json({"errorCode": "1006", "errorMessage" : util.format("Invalid attribute type! Please provide a valid %s, for '%s' of the '%s' resource!", theType, theAttribute, theResource), "statusCode" : "400"});
                                return;
                        }
                        if (Number(req.body.doorCount) < 1 || Number(req.body.doorCount) > 8)
                        {
                                var min = 1;
                                var max = 8;
                                res.status(400).json({"errorCode": "1014", "errorMessage" : util.format("Invalid range! '%s' should be between '%d' and '%d' for '%s' resource!", theAttribute,min,max, theResource), "statusCode" : "400"});
                                return;
                        }
                        car.doorCount = req.body.doorCount;
                    }
                
                    if (key.trim() == "model")
                    {
                        if (req.body.model.trim().length > 18)
                        {
                                theLength = 18;
                                res.status(400).json({"errorCode": "1013", "errorMessage" : util.format("Invalid length for '%s' of '%s' resource! Length for this attribute should be less than '%d' characters.", theAttribute,theResource,theLength), "statusCode" : "400"});
                                return;
                        }
                        car.model = req.body.model;
                    }

                    if (key.trim() == "make")
                    {  
                        if (req.body.make.trim().length > 18)
                        {
                                theLength= 18;
                                res.status(400).json({"errorCode": "1013", "errorMessage" : util.format("Invalid length for '%s' of '%s' resource! Length for this attribute should be less than '%d' characters.", theAttribute,theResource,theLength), "statusCode" : "400"});
                                return;
                        }
                        car.make = req.body.make;
                    }
                    if (key.trim() == "license")
                    {

                        if (req.body.license.trim().length > 18)
                        {
                                theLength = 18;
                                res.status(400).json({"errorCode": "1013", "errorMessage" : util.format("Invalid length for '%s' of '%s' resource! Length for this attribute should be less than '%d' characters.", theAttribute,theResource,theLen), "statusCode" : "400"});
                                return;
                        }
                        car.license = req.body.license;
                    }
                
                }
                car.save(function(err){
                    if(err){
                        res.status(500).send(err);
                    }else{
                        res.json({"message" : "Car Updated", "carUpdated" : car});
                    }
                });
            }
        });
    })
    
    /**
     * DELETE call for the car entity (single).
     * @returns {object} A string message. (200 Status Code)
     * @throws Mongoose Database Error (500 Status Code)
     */
    .delete(function(req, res){

        Car.remove({
            _id : req.params.car_id
        }, function(err, car){
            //never gets to this...
            if (typeof req.params.car_id === "undefined")
            {
                res.status(404).json({"errorCode": "1015", "errorMessage" : util.format(" Missing expected parameter for %s!", theResource), "statusCode" : "404"});
                return;
            }

            if (car == null)
            {
                res.status(404).json({"errorCode": "1007", "errorMessage" : util.format(" No %s with ID='%s' exists!", theResource, req.params.car_id), "statusCode" : "404"});
                return;
            }
            if(err){
                if (err.name == "CastError")
                {
                    res.status(404).json({"errorCode": "1008", "errorMessage" : util.format("%s is an Invalid ID type for %s resource!", err.value,theResource), "statusCode" : "404"});
                    return;
                }
                res.status(500).send(err);
            }else{
                res.json({"message" : "Car Deleted"});
            }
        });
    });

module.exports = router;