/** 
 * Express Route: /passengers
 * @author Clark Jeria
 * @version 0.0.3
 */
var express = require('express');
var router = express.Router();
var util = require('util');

var theLength;
var theLength1;
var theLength2;
var theResource = "passenger";
var theAttribute;
var theFormat;

/**
 * Validate the phone number
 */
function isValidPhone(inputtxt) {
  var phoneno = /^\(?([0-9]{3})\)?[-]?([0-9]{3})[-]?([0-9]{4})$/;
  if(inputtxt.match(phoneno)) {
    return true;
  }
  else {
    return false;
  }
}

/**
 * Validate the email
 */
function isValidEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

var Passenger = require('../app/models/passenger');

router.route('/passengers') 
    /**
     * GET call for the passenger entity (multiple).
     * @returns {object} A list of passengers. (200 Status Code)
     * @throws Mongoose Database Error (500 Status Code)
     */
    .get(function(req, res){
        Passenger.find(function(err, passengers){
            if (passengers == "")
            {
                 res.status(400).json({"errorCode": "1009", "errorMessage" : util.format("No data exists for %s!", theResource), "statusCode" : "400"});
                 return;
            }
            if(err){
                res.status(500).send(err);
            }else{
                res.json(passengers);
            }

        });
    })
    /**
     * POST call for the passenger entity.
     * @param {string} firstName - The first name of the new passenger
     * @param {string} lastName - The last name of the new passenger
     * @param {date} dateOfBirth - The date of birth of the new passenger
     * @param {string} username - The username of the new passenger
     * @param {string} password - The password of the new passenger
     * @param {string} addressLine1 - The address line 1 of the new passenger
     * @param {string} addressLine2 - The address line 2 of the new passenger
     * @param {string} city - The city of the new passenger
     * @param {string} state - The state of the new passenger
     * @param {number} zip - The zip code of the new passenger
     * @param {number} phoneNumber - The phone number of the new passenger
     * @returns {object} A message and the passenger created. (201 Status Code)
     * @throws Mongoose Database Error (500 Status Code)
     */
    .post(function(req, res){

        var passenger = new Passenger();

        var x=0;
 
        for (var key in req.body){x++ }
        if (x==0)
        {
            res.status(400).json({"errorCode": "1001", "errorMessage" : util.format("Empty Request body for %s resource!", theResource), "statusCode" : "400"});
            return;
        }

       var json = req.body;
 
        for (var key in json) {
            if (key.trim() != "firstName" && key.trim() !="lastName" && key.trim() != "emailAddress" && key.trim() != "password" && key.trim() != "addressLine1" &&
                key.trim() != "addressLine2" && key.trim() != "city" && key.trim() != "state" && key.trim() != "zip" && key.trim() != "phoneNumber")
            {
                res.status(400).json({"errorCode": "1003", "errorMessage" : util.format("Invalid attribute '%s' for the '%s' resource!", key, theResource), "statusCode" : "400"});
                return;
            }

            if (json[key] instanceof Array)
            {
                res.status(400).json({"errorCode": "1005", "errorMessage" : util.format("Duplicate attribute for '%s' of the '%s' resource!", key, theResource), "statusCode" : "400"});
                return;
            }  

            if (json[key] == "" && (key.trim() != "addressLine1" && key.trim() != "addressLine2" && key.trim() != "city" && key.trim() != "state" && key.trim() != "zip"))
            {
                res.status(400).json({"errorCode": "1004", "errorMessage" : util.format("Missing attribute value! Please provide a value for '%s' of the '%s' resource!", key, theResource), "statusCode" : "400"});
                return;
            }

        }

        var myKeys = ["firstName","lastName","emailAddress","password","phoneNumber"]
        for (i = 0; i < myKeys.length; i++) {
            if(!req.body.hasOwnProperty(myKeys[i]))
            {
              res.status(400).json({"errorCode": "1002", "errorMessage" : util.format("Missing attribute! Please provide '%s' of the '%s' resource!", myKeys[i], theResource), "statusCode" : "400"});
              return;
            }
        }

        theAttribute = "emailAddress";
        theFormat = "Email address format"
        if (!isValidEmail(req.body.emailAddress))
        {
                res.status(400).json({"errorCode": "1043", "errorMessage" : util.format("Invalid format for '%s' of '%s' resource! accepted format is: '%s'.", theAttribute,theResource,theFormat), "statusCode" : "400"});
                return;
        }

        theAttribute = "phoneNumber";
        theFormat = "XXX-XX-XXXX";
        if (!isValidPhone(req.body.phoneNumber))
        {
                res.status(400).json({"errorCode": "1043", "errorMessage" : util.format("Invalid format for '%s' of '%s' resource! accepted format is: '%s'.", theAttribute,theResource,theFormat), "statusCode" : "400"});
                return;
        }


        theAttribute = "firstName";
        if (req.body.firstName.trim().length > 15)
        {
                theLength = 15;
                res.status(400).json({"errorCode": "1013", "errorMessage" : util.format("Invalid length for '%s' of '%s' resource! Length for this attribute should be less than '%d' characters.", theAttribute,theResource,theLength), "statusCode" : "400"});
                return;
        }
        theAttribute = "lastName";
        if (req.body.lastName.trim().length > 15)
        {
                theLength = 15;
                res.status(400).json({"errorCode": "1013", "errorMessage" : util.format("Invalid length for '%s' of '%s' resource! Length for this attribute should be less than '%d' characters.", theAttribute,theResource,theLength), "statusCode" : "400"});
                return;
        }
        theAttribute = "pasword";
        if (req.body.password.trim().length > 16 || req.body.password.trim().length < 8)
        {
                theLength1 = 16;
                theLength2 = 8;
                res.status(400).json({"errorCode": "1023", "errorMessage" : util.format("Invalid length range for '%s' of '%s' resource! Length for this attribute should be between '%d' and '%d' characters.", theAttribute,theResource,theLength1,theLength2), "statusCode" : "400"});
                return;
        }
        
        if (typeof req.body.addressLine1 !== "undefined")
        {
            theAttribute = "addressLine1";
            if (req.body.addressLine1.trim().length > 50)
            {
                    theLength = 50;
                    res.status(400).json({"errorCode": "1013", "errorMessage" : util.format("Invalid length for '%s' of '%s' resource! Length for this attribute should be less than '%d' characters.", theAttribute,theResource,theLength), "statusCode" : "400"});
                    return;
            }
            passenger.addressLine1 = req.body.addressLine1;
        }
        
        if (typeof req.body.addressLine2 !== "undefined")
        {
            theAttribute = "addressLine2";
            if (req.body.addressLine2.trim().length > 50)
            {
                    theLength = 50;
                    res.status(400).json({"errorCode": "1013", "errorMessage" : util.format("Invalid length for '%s' of '%s' resource! Length for this attribute should be less than '%d' characters.", theAttribute,theResource,theLength), "statusCode" : "400"});
                    return;
            }
            passenger.addressLine2 = req.body.addressLine2;
        }
        if (typeof req.body.city !== "undefined")
        {
            theAttribute = "city";
            if (req.body.city.trim().length > 50)
            {
                    theLength = 50;
                    res.status(400).json({"errorCode": "1013", "errorMessage" : util.format("Invalid length for '%s' of '%s' resource! Length for this attribute should be less than '%d' characters.", theAttribute,theResource,theLength), "statusCode" : "400"});
                    return;
            }
            passenger.city = req.body.city;
        }
        if (typeof req.body.state !== "undefined")
        {
            theAttribute = "state";
            if (req.body.state.trim().length != 2)
            {
                    theLength = 2;
                    res.status(400).json({"errorCode": "1033", "errorMessage" : util.format("Invalid length for '%s' of '%s' resource! Length for this attribute should be '%d' characters.", theAttribute,theResource,theLength), "statusCode" : "400"});
                    return;
            }
            passenger.state = req.body.state;
        }
        if (typeof req.body.zip !== "undefined")
        {
            theAttribute = "zip";
            if (req.body.zip.trim().length != 5)
            {
                    theLength = 5;
                    res.status(400).json({"errorCode": "1033", "errorMessage" : util.format("Invalid length for '%s' of '%s' resource! Length for this attribute should be '%d' characters.", theAttribute,theResource,theLength), "statusCode" : "400"});
                    return;
            }
            passenger.zip = req.body.zip;
        }

        passenger.firstName = req.body.firstName;
        passenger.lastName = req.body.lastName;
        passenger.emailAddress = req.body.emailAddress;
        passenger.password = req.body.password;
        passenger.phoneNumber = req.body.phoneNumber;

        passenger.save(function(err){
            if(err){
                res.status(500).send(err);
            }else{
                res.status(201).json(passenger);
            }
        });
    });

/** 
 * Express Route: /passengers/:passenger_id
 * @param {string} passenger_id - Id Hash of passenger Object
 */
router.route('/passengers/:passenger_id')
    /**
     * GET call for the passenger entity (single).
     * @returns {object} the passenger with Id passenger_id. (200 Status Code)
     * @throws Mongoose Database Error (500 Status Code)
     */
    .get(function(req, res){
        Passenger.findById(req.params.passenger_id, function(err, passenger){
            if(err){
                if (err.name == "CastError")    
                {
                    res.status(404).json({"errorCode": "1008", "errorMessage" : util.format("%s is an Invalid ID type for %s resource!", err.value,theResource), "statusCode" : "404"});
                    return;
                }
                res.status(500).send(err);
            }else{
                if (passenger == null)
                {
                    res.status(404).json({"errorCode": "1007", "errorMessage" : util.format(" No %s with ID='%s' exists!", theResource, req.params.passenger_id), "statusCode" : "404"});
                    return;
                }
                res.json(passenger);
            }
        });  
    })
    /**
     * PATCH call for the passenger entity (single).
     * @param {string} firstName - The first name of the new passenger
     * @param {string} lastName - The last name of the new passenger
     * @param {date} dateOfBirth - The date of birth of the new passenger
     * @param {string} username - The username of the new passenger
     * @param {string} password - The password of the new passenger
     * @param {string} addressLine1 - The address line 1 of the new passenger
     * @param {string} addressLine2 - The address line 2 of the new passenger
     * @param {string} city - The city of the new passenger
     * @param {string} state - The state of the new passenger
     * @param {number} zip - The zip code of the new passenger
     * @param {number} phoneNumber - The phone number of the new passenger
     * @returns {object} A message and the passenger updated. (200 Status Code)
     * @throws Mongoose Database Error (500 Status Code)
     */
    .patch(function(req, res){
        Passenger.findById(req.params.passenger_id, function(err, passenger){
            if(err){
                 if (err.name == "CastError")    
                {
                    res.status(400).json({"errorCode": "1008", "errorMessage" : util.format("%s is an Invalid ID type for %s resource!", err.value,theResource), "statusCode" : "400"});
                    return;
                }
                res.status(500).send(err);
            }else{

                if (passenger == null)
                {
                    res.status(400).json({"errorCode": "1007", "errorMessage" : util.format(" No %s with ID='%s' exists!", theResource, req.params.passenger_id), "statusCode" : "400"});
                    return;
                }

        var x=0;
 
        for (var key in req.body){x++ }
        if (x==0)
        {
            res.status(400).json({"errorCode": "1001", "errorMessage" : util.format("Empty Request body for %s resource!", theResource), "statusCode" : "400"});
            return;
        }

       var json = req.body;
 
        for (var key in json) {
            if (key.trim() != "firstName" && key.trim() !="lastName" && key.trim() != "emailAddress" && key.trim() != "password" && key.trim() != "addressLine1" &&
                key.trim() != "addressLine2" && key.trim() != "city" && key.trim() != "state" && key.trim() != "zip" && key.trim() != "phoneNumber")
            {
                res.status(400).json({"errorCode": "1003", "errorMessage" : util.format("Invalid attribute '%s' for the '%s' resource!", key, theResource), "statusCode" : "400"});
                return;
            }

            if (key.trim() == "password")
            {
                res.status(400).json({"errorCode": "1016", "errorMessage" : util.format("Not allowed! '%s' of the '%s' resource can't be provided here!", key, theResource), "statusCode" : "400"});
                return;
            }


            if (json[key] instanceof Array)
            {
                res.status(400).json({"errorCode": "1005", "errorMessage" : util.format("Duplicate attribute for '%s' of the '%s' resource!", key, theResource), "statusCode" : "400"});
                return;
            }    

            
            
            if (json[key] == "" && (key.trim() != "addressLine1" && key.trim() != "addressLine2" && key.trim() != "city" && key.trim() != "state" && key.trim() != "zip"))
            {
                res.status(400).json({"errorCode": "1004", "errorMessage" : util.format("Missing attribute value! Please provide a value for '%s' of the '%s' resource!", key, theResource), "statusCode" : "400"});
                return;
            }

        }

        for(var key in req.body) {
            if(req.body.hasOwnProperty(key)){
                if(key == 'firstName'){
                    theAttribute = "firstName";
                    if (req.body.firstName.trim().length > 15)
                    {
                            theLength = 15;
                            res.status(400).json({"errorCode": "1013", "errorMessage" : util.format("Invalid length for '%s' of '%s' resource! Length for this attribute should be less than '%d' characters.", theAttribute,theResource,theLength), "statusCode" : "400"});
                            return;
                    }
                    passenger.firstName = req.body.firstName;
                }

                if(key == 'lastName'){
                    theAttribute = "lastName";
                    if (req.body.lastName.trim().length > 15)
                    {
                            theLength = 15;
                            res.status(400).json({"errorCode": "1013", "errorMessage" : util.format("Invalid length for '%s' of '%s' resource! Length for this attribute should be less than '%d' characters.", theAttribute,theResource,theLength), "statusCode" : "400"});
                            return;
                    }
                    passenger.lastName = req.body.lastName;
                }


                if(key == 'emailAddress'){
                        theAttribute = "emailAddress";
                        theFormat = "Email address format"
                        if (!isValidEmail(req.body.emailAddress))
                        {
                                res.status(400).json({"errorCode": "1043", "errorMessage" : util.format("Invalid format for '%s' of '%s' resource! accepted format is: '%s'.", theAttribute,theResource,theFormat), "statusCode" : "400"});
                                return;
                        }
                        passenger.emailAddress = req.body.emailAddress;
                }

                if(key == 'phoneNumber'){
                        theAttribute = "phoneNumber";
                        theFormat = "XXX-XX-XXXX";
                        if (!isValidPhone(req.body.phoneNumber))
                        {
                                res.status(400).json({"errorCode": "1043", "errorMessage" : util.format("Invalid format for '%s' of '%s' resource! accepted format is: '%s'.", theAttribute,theResource,theFormat), "statusCode" : "400"});
                                return;
                        }
                        passenger.phoneNumber = req.body.phoneNumber;
                }


                if(key == 'addressLine1'){
                        if (typeof req.body.addressLine1 !== "undefined")
                        {
                            theAttribute = "addressLine1";
                            if (req.body.addressLine1.trim().length > 50)
                            {
                                    theLength = 50;
                                    res.status(400).json({"errorCode": "1013", "errorMessage" : util.format("Invalid length for '%s' of '%s' resource! Length for this attribute should be less than '%d' characters.", theAttribute,theResource,theLength), "statusCode" : "400"});
                                    return;
                            }
                            passenger.addressLine1 = req.body.addressLine1;
                        }
                } 

                if (key == 'addressLine2')
                {            
                    if (typeof req.body.addressLine2 !== "undefined")
                    {
                        theAttribute = "addressLine2";
                        if (req.body.addressLine2.trim().length > 50)
                        {
                                theLength = 50;
                                res.status(400).json({"errorCode": "1013", "errorMessage" : util.format("Invalid length for '%s' of '%s' resource! Length for this attribute should be less than '%d' characters.", theAttribute,theResource,theLength), "statusCode" : "400"});
                                return;
                        }
                        passenger.addressLine2 = req.body.addressLine2;
                    }
                }

                if (key == 'city')
                { 
                    if (typeof req.body.city !== "undefined")
                    {
                        theAttribute = "city";
                        if (req.body.city.trim().length > 50)
                        {
                                theLength = 50;
                                res.status(400).json({"errorCode": "1013", "errorMessage" : util.format("Invalid length for '%s' of '%s' resource! Length for this attribute should be less than '%d' characters.", theAttribute,theResource,theLength), "statusCode" : "400"});
                                return;
                        }
                        passenger.city = req.body.city;
                    }
                }

                if (key == 'state')
                {               
                    if (typeof req.body.state !== "undefined")
                    {
                        theAttribute = "state";
                        if (req.body.state.trim().length != 2)
                        {
                                theLength = 2;
                                res.status(400).json({"errorCode": "1033", "errorMessage" : util.format("Invalid length for '%s' of '%s' resource! Length for this attribute should be '%d' characters.", theAttribute,theResource,theLength), "statusCode" : "400"});
                                return;
                        }
                        passenger.state = req.body.state;
                    }
                }

                if (key == 'zip')
                {              
                    if (typeof req.body.zip !== "undefined")
                    {
                        theAttribute = "zip";
                        if (req.body.zip.trim().length != 5)
                        {
                                theLength = 5;
                                res.status(400).json({"errorCode": "1033", "errorMessage" : util.format("Invalid length for '%s' of '%s' resource! Length for this attribute should be '%d' characters.", theAttribute,theResource,theLength), "statusCode" : "400"});
                                return;
                        }
                        passenger.zip = req.body.zip;
                    }
                }
            }
        }


        passenger.save(function(err){
            if(err){
                res.status(500).send(err);
            }else{
                res.json({"message" : "Passenger Updated", "passengerUpdated" : passenger});
            }
        });
            }
        });
    })
    /**
     * DELETE call for the passenger entity (single).
     * @returns {object} A string message. (200 Status Code)
     * @throws Mongoose Database Error (500 Status Code)
     */
    .delete(function(req, res){
        Passenger.remove({
            _id : req.params.passenger_id
        }, function(err, passenger){

            if (typeof req.params.passenger_id === "undefined")
            {
                es.status(404).json({"errorCode": "1015", "errorMessage" : util.format(" Missing expected parameter for %s!", theResource), "statusCode" : "404"});
                return;
            }

            if (passenger == null)
            {
                res.status(404).json({"errorCode": "1007", "errorMessage" : util.format(" No %s with ID='%s' exists!", theResource, req.params.passenger_id), "statusCode" : "404"});
                return;
            }
            if(err){
                if (err.name == "CastError")
                {
                    res.status(404).json({"errorCode": "1008", "errorMessage" : util.format("%s is an Invalid ID type for %s resource!", err.value,theResource), "statusCode" : "404"});
                    return;
                }
                res.status(500).send(err);

            }else{
                res.json({"message" : "Passenger Deleted"});
            }
        });
    });

module.exports = router;