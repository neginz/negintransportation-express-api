/** 
 * Express Route: /rides
 * @author Clark Jeria
 * @version 0.0.3
 */
var express = require('express');
var router = express.Router();
var util = require('util');
var mongoose     = require('mongoose');
var Ride = require('../app/models/ride');
var theResource = "Ride";
var theAttribute;

/**
 * Here you must add the routes for the Ride entity
 * /rides/:id/routePoints (POST)
 * /rides/:id/routePoints (GET)
 * /rides/:id/routePoint/current (GET)
 */

router.route('/rides') 
    /**
     * GET call for the ride entity (multiple).
     * @returns {object} A list of rides. (200 Status Code)
     * @throws Mongoose Database Error (500 Status Code)
     */
    .get(function(req, res){

        Ride.find(function(err, rides){
            
            if (rides == "")
            {
                 res.status(404).json({"errorCode": "1009", "errorMessage" : util.format("No data exists for %s!", theResource), "statusCode" : "404"});
                 return;
            }
            if(err){
                res.status(500).send(err);
            }else{
                res.json(rides);
            }
        });
    })
    /**
     * POST call for the ride.
     * @param {object} passenger - The passenger of the new ride
     * @param {object} driver - The driver of the new ride
     * @param {string} rideType - The rideType of the new ride
     * @param {object} startPoint - The startPoint of the new ride
     * @param {string} endPoint - The endPoint of the new ride
     * @param {object} requestTime - The requestTime of the new ride
     * @param {string} pickupTime - The pickupTime of the new ride
     * @param {object} dropOffTime - The dropOffTime of the new ride
     * @param {string} status - The status of the new ride
     * @param {string} fare - The fare of the new ride
     * @param {array of object} route - The route of the new ride
     * @returns {} A message and the ride created. (201 Status Code)
     * @throws Mongoose Database Error (500 Status Code)
     */
    .post(function(req, res){

        var ride = new Ride();
        var x=0;
 
        
        var myKeys = ["car","passenger","driver","startPoint","endPoint","requestTime","pickupTime","dropOffTime","status","fare","rideType"]
        for (i = 0; i < myKeys.length; i++) {
            if(!req.body.hasOwnProperty(myKeys[i]))
            {
              res.status(400).json({"errorCode": "1002", "errorMessage" : util.format("Missing attribute! Please provide '%s' of the '%s' resource!", myKeys[i], theResource), "statusCode" : "400"});
              return;
            }
        }

        if (req.body.rideType!= "ECONOMY" && req.body.rideType!="PREMIUM" && req.body.rideType != "EXECUTIVE" )
        {
            res.status(400).json({"errorCode": "1063", "errorMessage" : util.format("Must provide a valid attribute '%s' for the '%s' resource!", "Ride Type", theResource), "statusCode" : "400"});
            return;
        }

        if (req.body.status != "REQUESTED" && req.body.status != "AWAITING_DRIVER" && req.body.status != "DRIVE_ASSIGNED" &&
            req.body.status != "IN_PROGRESS" && req.body.status !="ARRIVED" && req.body.status != "CLOSED")
        {
                res.status(400).json({"errorCode": "1063", "errorMessage" : util.format("Must provide a valid attribute '%s' for the '%s' resource!", "Status", theResource), "statusCode" : "400"});
                return; 
        }

        var theFormat = "2010-01-01"
        if (new Date(req.body.pickupTime).getTime() <= 0 || isNaN(new Date(req.body.pickupTime).getTime()))
        {
                theAttribute = "Pickup Time"
                res.status(422).json({"errorCode": "1043", "errorMessage" : util.format("Invalid format for '%s' of '%s' resource! accepted format is: '%s'.", theAtt,theResource,theFormat), "statusCode" : "422"});
                return;
 
        }

        if (new Date(req.body.dropOffTime).getTime() <= 0 || isNaN(new Date(req.body.dropOffTime).getTime()))
        {
                theAttribute = "Drop off Time"
                res.status(422).json({"errorCode": "1043", "errorMessage" : util.format("Invalid format for '%s' of '%s' resource! accepted format is: '%s'.", theAtt,theResource,theFormat), "statusCode" : "422"});
                return;
        }

        if (new Date(req.body.requestTime).getTime() <= 0 || isNaN(new Date(req.body.requestTime).getTime()))
        {
                theAttribute = "Request Time"
                res.status(422).json({"errorCode": "1043", "errorMessage" : util.format("Invalid format for '%s' of '%s' resource! accepted format is: '%s'.", theAtt,theResource,theFormat), "statusCode" : "422"});
                return;
 
        }

        theAttribute = "fare";
        if (isNaN(Number(req.body.fare)))
        {
                var theType = "Number"
                res.status(400).json({"errorCode": "1006", "errorMessage" : util.format("Invalid attribute type! Please provide a valid %s, for '%s' of the '%s' resource!", theType, theAtt, theResource), "statusCode" : "400"});
                return;
        }


        ride.car = mongoose.Types.ObjectId(req.body.car);
        ride.passenger = mongoose.Types.ObjectId(req.body.passenger);
        ride.driver = mongoose.Types.ObjectId(req.body.driver);
        ride.rideType = req.body.rideType;
        ride.startPoint.lat = req.body.startPoint.lat;
        ride.startPoint.long = req.body.startPoint.long;
        ride.endPoint.lat = req.body.startPoint.lat;
        ride.endPoint.long = req.body.startPoint.long;
        ride.requestTime = new Date(req.body.requestTime).getTime();
        ride.pickupTime = new Date(req.body.pickupTime).getTime();
        ride.dropOffTime = new Date(req.body.dropOffTime).getTime();
        ride.status = req.body.status;
        ride.fare = req.body.fare;

        ride.route.push({"long":req.body.route.long, "lat":req.body.route.lat});

        ride.save(function(err){
            if(err){
                res.status(500).send(err);
            }else{
                res.status(201).json(ride);
            }
        });
    });




router.route('/rides/:ride_id/routePoints')
    /**
     * GET call for the ride (single).
     * @returns route Pointes {object} of the ride with Id ride_id. (200 Status Code)
     * @throws Mongoose Database Error (500 Status Code)
     */
    .get(function(req, res){

        Ride.findById(req.params.ride_id, function(err, ride){
            
            if(err){
                if (err.name == "CastError")    
                {
                    res.status(404).json({"errorCode": "1008", "errorMessage" : util.format("%s is an Invalid ID type for %s resource!", err.value,theResource), "statusCode" : "404"});
                    return;
                }
                res.status(500).send(err);
            }else{
                if (ride == null)
                {
                    res.status(404).json({"errorCode": "1007", "errorMessage" : util.format(" No %s with ID='%s' exists!", theResource, req.params.ride_id), "statusCode" : "404"});
                    return;
                }

                res.json(ride.route);
            }

        })  
    })
    /**
     * POST call for the ride. to post a new route
     * @param {array of object} route - The route of the new ride
     * @returns {} A message and the ride created. (201 Status Code)
     * @throws Mongoose Database Error (500 Status Code)
     */
    .post(function(req, res){

        Ride.findById(req.params.ride_id, function(err, ride){
            
            if(err){
                if (err.name == "CastError")    
                {
                    res.status(404).json({"errorCode": "1008", "errorMessage" : util.format("%s is an Invalid ID type for %s resource!", err.value,theResource), "statusCode" : "404"});
                    return;
                }
                    res.status(500).send(err);
            }else{
                    if (ride == null)
                    {
                        res.status(404).json({"errorCode": "1007", "errorMessage" : util.format(" No %s with ID='%s' exists!", theResource, req.params.ride_id), "statusCode" : "404"});
                        return;
                    }
                    ride.route.push({"long":req.body.route.long, "lat":req.body.route.lat});
                    res.json(ride.route);
                    ride.save(function(err){
                    if(err){
                        res.status(500).send(err);
                    }else{
                        res.status(201).json(ride);
                        }})
                    }
            })
    });
    
    router.route('/rides/:ride_id/routePoint/current')
    /**
     * GET call for the ride (single).
     * @returns current position {object} of the ride with Id ride_id. (200 Status Code)
     * @throws Mongoose Database Error (500 Status Code)
     */
    .get(function(req, res){

        Ride.findById(req.params.ride_id, function(err, ride){
            
            if(err){
                if (err.name == "CastError")    
                {
                    res.status(404).json({"errorCode": "1008", "errorMessage" : util.format("%s is an Invalid ID type for %s resource!", err.value,theResource), "statusCode" : "404"});
                    return;
                }
                res.status(500).send(err);
            }else{
                if (ride == null)
                {
                    res.status(404).json({"errorCode": "1007", "errorMessage" : util.format(" No %s with ID='%s' exists!", theResource, req.params.ride_id), "statusCode" : "404"});
                    return;
                }
                res.json(ride.route[ride.route.length-1]);
            }

        })  
    });
   

module.exports = router;



