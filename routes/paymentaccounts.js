/** 
 * Express Route: /paymentaccounts
 * @author Clark Jeria
 * @version 0.0.3
 */
var express = require('express');
var router = express.Router();
var util = require('util');

var theLength;
var theResource = "payment account";
var theAttribute;
var theFormat;

var PaymentAccount = require('../app/models/paymentaccount');

router.route('/paymentaccounts') 
    /**
     * GET call for the paymentAccount entity (multiple).
     * @returns {object} A list of paymentAccounts. (200 Status Code)
     * @throws Mongoose Database Error (500 Status Code)
     */
    .get(function(req, res){

        PaymentAccount.find(function(err, paymentAccounts){
            if (paymentAccounts == "")
            {
                 res.status(422).json({"errorCode": "1009", "errorMessage" : util.format("No data exists for %s!", theResource), "statusCode" : "422"});
                 return;
            }
            if(err){
                res.status(500).send(err);
            }else{
                res.json(paymentAccounts);
            }
        });
    })

    /**
     * POST call for the paymentAccount entity.
     * @param {string} accountType - The account type of the new paymentAccount
     * @param {integer} accountNumber - The account number of the new paymentAccount
     * @param {date} expirationDate - The expiration date of the new paymentAccount
     * @param {string} nameOnAccount - The name on account of the new paymentAccount
     * @param {string} bank - The bank of the new paymentAccount
     * @returns {object} A message and the paymentAccount created. (201 Status Code)
     * @throws Mongoose Database Error (500 Status Code)
     */
    .post(function(req, res){

        var x=0;
 
        for (var key in req.body){x++ }
        if (x==0)
        {
            res.status(422).json({"errorCode": "1001", "errorMessage" : util.format("Empty Request body for %s resource!", theResource), "statusCode" : "422"});
            return;
        }

       var json = req.body;
 
        for (var key in json) {
            if (key.trim() != "accountType" && key.trim() !="accountNumber" && key.trim() != "expirationDate" && key.trim() != "nameOnAccount" && key.trim() != "bank")
            {
                res.status(422).json({"errorCode": "1003", "errorMessage" : util.format("Invalid attribute '%s' for the '%s' resource!", key, theResource), "statusCode" : "422"});
                return;
            }

            if (json[key] == "")
            {
                res.status(422).json({"errorCode": "1003", "errorMessage" : util.format("Missing attribute value! Please provide a value for '%s' of the '%s' resource!", key, theResource), "statusCode" : "422"});
                return;
            }

            if (json[key] instanceof Array)
            {
                res.status(422).json({"errorCode": "1005", "errorMessage" : util.format("Duplicate attribute for '%s' of the '%s' resource!", key, theResource), "statusCode" : "422"});
                return;
            }       

        }

        var myKeys = ["accountType","accountNumber","expirationDate","nameOnAccount","bank"]
        for (i = 0; i < myKeys.length; i++) {
            if(!req.body.hasOwnProperty(myKeys[i]))
            {
              res.status(422).json({"errorCode": "1002", "errorMessage" : util.format("Missing attribute! Please provide '%s' of the '%s' resource!", myKeys[i], theResource), "statusCode" : "422"});
              return;
            }
        }


        theAttribute = "accountNumber";
        theFormat = "18 digit number."
        if (isNaN(Number(req.body.accountNumber)) || req.body.accountNumber.trim().length != 18)
        {
                theType = "number"
                res.status(422).json({"errorCode": "1043", "errorMessage" : util.format("Invalid format for '%s' of '%s' resource! accepted format is: '%s'.", theAttribute,theResource,theFormat), "statusCode" : "422"});
                return;
        }
 
        theAttribute = "accountType";
        if (req.body.accountType.trim().length != 18)
        {
                theLength = 18;
                res.status(422).json({"errorCode": "1033", "errorMessage" : util.format("Invalid length for '%s' of '%s' resource! Length for this attribute should be '%d' characters.", theAttribute,theResource,theLength), "statusCode" : "422"});
                return;
        }

        theAttribute = "nameOnAccount";
        if (req.body.nameOnAccount.trim().length != 18)
        {
                theLength = 18;
                res.status(422).json({"errorCode": "1033", "errorMessage" : util.format("Invalid length for '%s' of '%s' resource! Length for this attribute should be '%d' characters.", theAttribute,theResource,theLength), "statusCode" : "422"});
                return;
        }


        theAttribute = "bank";
        if (req.body.bank.trim().length != 18)
        {
                theLength = 18;
                res.status(422).json({"errorCode": "1033", "errorMessage" : util.format("Invalid length for '%s' of '%s' resource! Length for this attribute should be '%d' characters.", theAttribute,theResource,theLength), "statusCode" : "422"});
                return;
        }

        theAttribute = "expirationDate";
        theFormat = "Timestamp (2016-10-01)"
        
        if (new Date(req.body.expirationDate).getTime() <= 0 || isNaN(new Date(req.body.expirationDate).getTime()))
        {
                theType = "number"
                res.status(422).json({"errorCode": "1043", "errorMessage" : util.format("Invalid format for '%s' of '%s' resource! accepted format is: '%s'.", theAttribute,theResource,theFormat), "statusCode" : "422"});
                return;
 
        }

        var paymentAccount = new PaymentAccount();
        paymentAccount.accountType = req.body.accountType;
        paymentAccount.accountNumber = req.body.accountNumber;
        paymentAccount.expirationDate = new Date(req.body.expirationDate).getTime();
        paymentAccount.nameOnAccount = req.body.nameOnAccount;
        paymentAccount.bank = req.body.bank;

        paymentAccount.save(function(err){
            if(err){
                res.status(500).send(err);
            }else{
                res.status(201).json({"message" : "PaymentAccount Created", "paymentAccountCreated" : paymentAccount});
            }
        });
    });

/** 
 * Express Route: /paymentaccounts/:paymentaccount_id
 * @param {string} paymentaccount_id - Id Hash of PaymentAccount Object
 */
router.route('/paymentaccounts/:paymentaccount_id')
    /**
     * GET call for the paymentAccount entity (single).
     * @returns {object} the paymentaccount with Id paymentaccount_id. (200 Status Code)
     * @throws Mongoose Database Error (500 Status Code)
     */
    .get(function(req, res){        

        PaymentAccount.findById(req.params.paymentaccount_id, function(err, paymentAccount){

            if(err){
                if (err.name == "CastError")    
                {
                    res.status(422).json({"errorCode": "1008", "errorMessage" : util.format("%s is an Invalid ID type for %s resource!", err.value,theResource), "statusCode" : "422"});
                    return;
                }
                res.status(500).send(err);
            }else{
                if (paymentAccount == null)
                {
                    res.status(422).json({"errorCode": "1007", "errorMessage" : util.format(" No %s with ID='%s' exists!", theResource, req.params.paymentaccount_id), "statusCode" : "422"});
                    return;
                }
                res.json(paymentAccount);
            }
        });  
    })
    
    /**
     * PATCH call for the paymentAccount entity (single).
     * @param {string} accountType - The account type of the new paymentAccount
     * @param {integer} accountNumber - The account number of the new paymentAccount
     * @param {date} expirationDate - The expiration date of the new paymentAccount
     * @param {string} nameOnAccount - The name on account of the new paymentAccount
     * @param {string} bank - The bank of the new paymentAccount
     * @returns {object} A message and the paymentAccount created. (201 Status Code)
     * @returns {object} A message and the paymentaccount updated. (200 Status Code)
     * @throws Mongoose Database Error (500 Status Code)
     */
    .patch(function(req, res){

        PaymentAccount.findById(req.params.paymentaccount_id, function(err, paymentAccount){
            if(err){
                 if (err.name == "CastError")    
                {
                    res.status(422).json({"errorCode": "1008", "errorMessage" : util.format("%s is an Invalid ID type for %s resource!", err.value,theResource), "statusCode" : "422"});
                    return;
                }
                res.status(500).send(err);
                
            }else{

                if (paymentAccount == null)
                {
                    res.status(422).json({"errorCode": "1007", "errorMessage" : util.format(" No %s with ID='%s' exists!", theResource, req.params.paymentaccount_id), "statusCode" : "422"});
                    return;
                }

        var x=0;
 
        for (var key in req.body){x++ }
        if (x==0)
        {
            res.status(422).json({"errorCode": "1001", "errorMessage" : util.format("Empty Request body for %s resource!", theResource), "statusCode" : "422"});
            return;
        }

        var json = req.body;
 
        for (var key in json) {
            if (key.trim() != "accountType" && key.trim() !="accountNumber" && key.trim() != "expirationDate" && key.trim() != "nameOnAccount" && key.trim() != "bank")
            {
                res.status(422).json({"errorCode": "1003", "errorMessage" : util.format("Invalid attribute '%s' for the '%s' resource!", key, theResource), "statusCode" : "422"});
                return;
            }

            if (json[key] == "")
            {
                res.status(422).json({"errorCode": "1003", "errorMessage" : util.format("Missing attribute value! Please provide a value for '%s' of the '%s' resource!", key, theResource), "statusCode" : "422"});
                return;
            }

            if (json[key] instanceof Array)
            {
                res.status(422).json({"errorCode": "1005", "errorMessage" : util.format("Duplicate attribute for '%s' of the '%s' resource!", key, theResource), "statusCode" : "422"});
                return;
            }       

        }

        for(var key in req.body) {
            if(req.body.hasOwnProperty(key)){
                if(key == 'accountNumber'){
                    theAttribute = "accountNumber";
                    theFormat = "18 digit number."
                    if (isNaN(Number(req.body.accountNumber)) || req.body.accountNumber.trim().length != 18)
                    {
                            theType = "number"
                            res.status(422).json({"errorCode": "1043", "errorMessage" : util.format("Invalid format for '%s' of '%s' resource! accepted format is: '%s'.", theAttribute,theResource,theFormat), "statusCode" : "422"});
                            return;
                    }
                    paymentAccount.accountNumber = req.body.accountNumber;
                }
                if(key == 'accountType'){
                    theAttribute = "accountType";
                    if (req.body.accountType.trim().length != 18)
                    {
                            theLength = 18;
                            res.status(422).json({"errorCode": "1033", "errorMessage" : util.format("Invalid length for '%s' of '%s' resource! Length for this attribute should be '%d' characters.", theAttribute,theResource,theLength), "statusCode" : "422"});
                            return;
                    }
                    paymentAccount.accountType = req.body.accountType;
                }

                if(key == 'nameOnAccount'){
                    theAttribute = "nameOnAccount";
                    if (req.body.nameOnAccount.trim().length != 18)
                    {
                            theLength = 18;
                            res.status(422).json({"errorCode": "1033", "errorMessage" : util.format("Invalid length for '%s' of '%s' resource! Length for this attribute should be '%d' characters.", theAttribute,theResource,theLength), "statusCode" : "422"});
                            return;
                    }
                    paymentAccount.nameOnAccount = req.body.nameOnAccount;
                }
                if(key == 'bank'){
                    theAttribute = "bank";
                    if (req.body.bank.trim().length != 18)
                    {
                            theLength = 18;
                            res.status(422).json({"errorCode": "1033", "errorMessage" : util.format("Invalid length for '%s' of '%s' resource! Length for this attribute should be '%d' characters.", theAttribute,theResource,theLength), "statusCode" : "422"});
                            return;
                    }
                    paymentAccount.bank = req.body.bank;
                }

                if(key == 'expirationDate'){
                    theAttribute = "expirationDate";
                    theFormat = "Timestamp (2016-10-01)"
                    if (new Date(req.body.expirationDate).getTime() <= 0 || isNaN(new Date(req.body.expirationDate).getTime()))
                    {
                            theType = "number"
                            res.status(422).json({"errorCode": "1043", "errorMessage" : util.format("Invalid format for '%s' of '%s' resource! accepted format is: '%s'.", theAttribute,theResource,theFormat), "statusCode" : "422"});
                            return;
            
                    }
                    paymentAccount.expirationDate =  new Date(req.body.expirationDate).getTime();
                }
            }
        }

        paymentAccount.save(function(err){
            if(err){
                res.status(500).send(err);
            }else{
                res.json({"message" : "PaymentAccount Updated", "paymentAccountUpdated" : paymentAccount});
            }
        });
            }
        });
    })

    /**
     * DELETE call for the paymentaccount entity (single).
     * @returns {object} A string message. (200 Status Code)
     * @throws Mongoose Database Error (500 Status Code)
     */
    .delete(function(req, res){  
        PaymentAccount.remove({
            _id : req.params.paymentaccount_id
        }, function(err, paymentaccount){

            if (typeof req.params.paymentaccount_id === "undefined")
            {
                es.status(422).json({"errorCode": "1015", "errorMessage" : util.format(" Missing expected parameter for %s!", theResource), "statusCode" : "422"});
                return;
            }

            if (paymentaccount == null)
            {
                res.status(422).json({"errorCode": "1007", "errorMessage" : util.format(" No %s with ID='%s' exists!", theResource, req.params.paymentaccount_id), "statusCode" : "422"});
                return;
            }
            if(err){
                if (err.name == "CastError")
                {
                    res.status(422).json({"errorCode": "1008", "errorMessage" : util.format("%s is an Invalid ID type for %s resource!", err.value,theResource), "statusCode" : "422"});
                    return;
                }
                res.status(500).send(err);
            }else{
                res.json({"message" : "PaymentAccount Deleted"});
            }
        });
    });

module.exports = router;