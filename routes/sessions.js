var express = require('express');
var router = express.Router();
var util = require('util');
var mongoose = require('mongoose');

var CryptoJS=require("crypto-js");
var base64=require("js-base64").Base64;



router.route('/sessions')
    /**
     * POST call for session.
     * @param {string} username - username
     * @param {string} password - password
     * @returns {object} A token.
     * @throws Mongoose Database Error (500 Status Code)
     */
    .post(function(req, res){

        /**
         * This is the pretended valid username/password
         */
        username = "negin";
        password = "pass1234"
            
        if(!req.body.hasOwnProperty("username"))
        {
            res.status(400).json({"errorCode": "996", "errorMessage" : util.format("Please provied a %s!", "username"), "statusCode" : "400"});
            return;
        }

        if(!req.body.hasOwnProperty("password"))
        {
            res.status(400).json({"errorCode": "996", "errorMessage" : util.format("Please provied a %s!", "password"), "statusCode" : "400"});
            return;
        }

        currentUserName = req.body.username;
        currentPassword = req.body.password;

        if (currentUserName != username || currentPassword != password)
        {
            res.status(401).json({"errorCode": "997", "errorMessage" : util.format("Invalide %s!", "username or password"), "statusCode" : "401"});
            return; 
        }

        expiration = (parseInt(Date.now()/1000) + 3600);

        var hashPass = CryptoJS.HmacSHA1(clearString,"APP");
        var clearString = username+":"+expiration;
        var hashString = CryptoJS.HmacSHA1(clearString,"APP");
        var cryptString = CryptoJS.AES.encrypt(clearString+":"+hashString,"Secret").toString();

        /** generate a token and send it in response
         */
        response = {token: base64.encode(cryptString)}

        res.status(200).json(response);
});

module.exports = router;