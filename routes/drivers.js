/** 
 * Express Route: /drivers
 * @author Clark Jeria
 * @version 0.0.3
 */
var express = require('express');
var router = express.Router();
var util = require('util');

var theLength;
var theLength1;
var theLength2;
var theResource = "driver";
var theAttribute;
var theFormat;

var Driver = require('../app/models/driver');

/**
 * To Validate the Phone Number
 */
function isValidPhone(inputtxt) {
  var phoneno = /^\(?([0-9]{3})\)?[-]?([0-9]{3})[-]?([0-9]{4})$/;
  if(inputtxt.match(phoneno)) {
    return true;
  }
  else {
    return false;
  }
}

/**
 * To Validate the email
 */
function isValidEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}



router.route('/drivers') 
    /**
     * GET call for the driver entity (multiple).
     * @returns {object} A list of drivers. (200 Status Code)
     * @throws Mongoose Database Error (500 Status Code)
     */
    .get(function(req, res){

        Driver.find(function(err, drivers){
            if (drivers == "")
            {
                 res.status(422).json({"errorCode": "1009", "errorMessage" : util.format("No data exists for %s!", theResource), "statusCode" : "422"});
                 return;
            }
            if(err){
                res.status(500).send(err);
            }else{
                res.json(drivers);
            }
        });
    })

    /**
     * POST call for the driver entity.
     * @param {string} firstName - The first name of the new driver
     * @param {string} lastName - The last name of the new driver
     * @param {date} dateOfBirth - The date of birth of the new driver
     * @param {string} licenseType - The license type of the new driver
     * @param {string} username - The username of the new driver
     * @param {string} password - The password of the new driver
     * @param {string} addressLine1 - The address line 1 of the new driver
     * @param {string} addressLine2 - The address line 2 of the new driver
     * @param {string} city - The city of the new driver
     * @param {string} state - The state of the new driver
     * @param {number} zip - The zip code of the new driver
     * @param {number} phoneNumber - The phone number of the new driver
     * @returns {object} A message and the driver created. (201 Status Code)
     * @throws Mongoose Database Error (500 Status Code)
     */
    .post(function(req, res){

        var driver = new Driver();

        var x=0;
 
        for (var key in req.body){x++ }
        if (x==0)
        {
            res.status(400).json({"errorCode": "1001", "errorMessage" : util.format("Empty Request body for %s resource!", theResource), "statusCode" : "400"});
            return;
        }

       var json = req.body;
 
        for (var key in json) {
            if (key.trim() != "firstName" && key.trim() !="lastName" && key.trim() != "emailAddress" && key.trim() != "password" && key.trim() != "addressLine1" &&
                key.trim() != "addressLine2" && key.trim() != "city" && key.trim() != "state" && key.trim() != "zip" && key.trim() != "phoneNumber" && 
                key.trim() != "drivingLicense" && key.trim() != "licensedState")
            {
                res.status(400).json({"errorCode": "1003", "errorMessage" : util.format("Invalid attribute '%s' for the '%s' resource!", key, theResource), "statusCode" : "400"});
                return;
            }

            if (json[key] == "" && (key.trim() != "addressLine1" && key.trim() != "addressLine2" && key.trim() != "city" && key.trim() != "state" && key.trim() != "zip"))
            {
                res.status(400).json({"errorCode": "1003", "errorMessage" : util.format("Missing attribute value! Please provide a value for '%s' of the '%s' resource!", key, theResource), "statusCode" : "400"});
                return;
            }

            if (json[key] instanceof Array)
            {
                res.status(4200).json({"errorCode": "1005", "errorMessage" : util.format("Duplicate attribute for '%s' of the '%s' resource!", key, theResource), "statusCode" : "400"});
                return;
            }       

        }


        var myKeys = ["firstName","lastName","emailAddress","password","phoneNumber","drivingLicense","licensedState"]
        for (i = 0; i < myKeys.length; i++) {
            if(!req.body.hasOwnProperty(myKeys[i]))
            {
              res.status(400).json({"errorCode": "1002", "errorMessage" : util.format("Missing attribute! Please provide '%s' of the '%s' resource!", myKeys[i], theResource), "statusCode" : "400"});
              return;
            }
        }


        theAttribute = "emailAddress";
        theFormat = "Email address format"
        if (!isValidEmail(req.body.emailAddress))
        {
                res.status(400).json({"errorCode": "1043", "errorMessage" : util.format("Invalid format for '%s' of '%s' resource! accepted format is: '%s'.", theAttribute,theResource,theFormat), "statusCode" : "400"});
                return;
        }

        theAttribute = "phoneNumber";
        theFormat = "XXX-XX-XXXX";
        if (!isValidPhone(req.body.phoneNumber))
        {
                res.status(400).json({"errorCode": "1043", "errorMessage" : util.format("Invalid format for '%s' of '%s' resource! accepted format is: '%s'.", theAttribute,theResource,theFormat), "statusCode" : "400"});
                return;
        }


        theAttribute = "firstName";
        if (req.body.firstName.trim().length > 15)
        {
                theLength = 15;
                res.status(400).json({"errorCode": "1013", "errorMessage" : util.format("Invalid length for '%s' of '%s' resource! Length for this attribute should be less than '%d' characters.", theAttribute,theResource,theLength), "statusCode" : "400"});
                return;
        }
        theAttribute = "lastName";
        if (req.body.lastName.trim().length > 15)
        {
                theLength = 15;
                res.status(400).json({"errorCode": "1013", "errorMessage" : util.format("Invalid length for '%s' of '%s' resource! Length for this attribute should be less than '%d' characters.", theAttribute,theResource,theLength), "statusCode" : "400"});
                return;
        }
        theAttribute = "password";
        if (req.body.password.trim().length > 16 || req.body.password.trim().length < 8)
        {
                theLength1 = 16;
                theLength2 = 8;
                res.status(400).json({"errorCode": "1023", "errorMessage" : util.format("Invalid length range for '%s' of '%s' resource! Length for this attribute should be between '%d' and '%d' characters.", theAttribute,theResource,theLength1,theLength2), "statusCode" : "400"});
                return;
        }


        if (typeof req.body.addressLine1 !== "undefined")
        {
            theAttribute = "addressLine1";
            if (req.body.addressLine1.trim().length > 50)
            {
                    theLength = 50;
                    res.status(400).json({"errorCode": "1013", "errorMessage" : util.format("Invalid length for '%s' of '%s' resource! Length for this attribute should be less than '%d' characters.", theAttribute,theResource,theLength), "statusCode" : "400"});
                    return;
            }
            driver.addressLine1 = req.body.addressLine1;
        }
        
        if (typeof req.body.addressLine2 !== "undefined")
        {
            theAttribute = "addressLine2";
            if (req.body.addressLine2.trim().length > 50)
            {
                    theLength = 50;
                    res.status(400).json({"errorCode": "1013", "errorMessage" : util.format("Invalid length for '%s' of '%s' resource! Length for this attribute should be less than '%d' characters.", theAttribute,theResource,theLength), "statusCode" : "400"});
                    return;
            }
            driver.addressLine2 = req.body.addressLine2;
        }

        
        if (typeof req.body.city !== "undefined")
        {
            theAttribute = "city";
            if (req.body.city.trim().length > 50)
            {
                    theLength = 50;
                    res.status(400).json({"errorCode": "1013", "errorMessage" : util.format("Invalid length for '%s' of '%s' resource! Length for this attribute should be less than '%d' characters.", theAttribute,theResource,theLength), "statusCode" : "400"});
                    return;
            }
            driver.city = req.body.city;
        }
        if (typeof req.body.state !== "undefined")
        {
            theAttribute = "state";
            if (req.body.state.trim().length != 2)
            {
                    theLength = 2;
                    res.status(400).json({"errorCode": "1033", "errorMessage" : util.format("Invalid length for '%s' of '%s' resource! Length for this attribute should be '%d' characters.", theAttribute,theResource,theLength), "statusCode" : "400"});
                    return;
            }
            driver.state = req.body.state;
        }
        if (typeof req.body.zip !== "undefined")
        {
            theAttribute = "zip";
            if (req.body.zip.trim().length != 5)
            {
                    theLength = 5;
                    res.status(400).json({"errorCode": "1033", "errorMessage" : util.format("Invalid length for '%s' of '%s' resource! Length for this attribute should be '%d' characters.", theAttribute,theResource,theLength), "statusCode" : "400"});
                    return;
            }
            driver.zip = req.body.zip;
        }


        theAttribute = "drivingLicense";
        if (req.body.drivingLicense.trim().length < 8 || req.body.drivingLicense.trim().length > 16 )
        {
                theLength1 = 8;
                theLength2 = 16;
                res.status(400).json({"errorCode": "1023", "errorMessage" : util.format("Invalid length range for '%s' of '%s' resource! Length for this attribute should be between '%d' and '%d' characters.", theAttribute,theResource,theLength1,theLength2), "statusCode" : "400"});
                return;
        }

        theAttribute = "LicensedState";
        if (req.body.licensedState.trim().length != 2)
        {
                theLength = 5;
                res.status(400).json({"errorCode": "1033", "errorMessage" : util.format("Invalid length for '%s' of '%s' resource! Length for this attribute should be '%d' characters.", theAttribute,theResource,theLength), "statusCode" : "400"});
                return;
        }

        driver.firstName = req.body.firstName;
        driver.lastName = req.body.lastName;
        driver.emailAddress = req.body.emailAddress;
        driver.password = req.body.password;
        driver.phoneNumber = req.body.phoneNumber;
        driver.licensedState = req.body.licensedState;
        driver.drivingLicense = req.body.drivingLicense;

        driver.save(function(err){
            if(err){
                res.status(500).send(err);
            }else{
                res.status(201).json(driver);
                
            }
        });
    });

/** 
 * Express Route: /drivers/:driver_id
 * @param {string} driver_id - Id Hash of driver Object
 */
router.route('/drivers/:driver_id')
    /**
     * GET call for the driver entity (single).
     * @returns {object} the driver with Id driver_id. (200 Status Code)
     * @throws Mongoose Database Error (500 Status Code)
     */
    .get(function(req, res){
        Driver.findById(req.params.driver_id, function(err, driver){
            if(err){
                if (err.name == "CastError")    
                {
                    res.status(404).json({"errorCode": "1008", "errorMessage" : util.format("%s is an Invalid ID type for %s resource!", err.value,theResource), "statusCode" : "404"});
                    return;
                }
                res.status(500).send(err);
            }else{
                if (driver == null)
                {
                    res.status(404).json({"errorCode": "1007", "errorMessage" : util.format(" No %s with ID='%s' exists!", theResource, req.params.driver_id), "statusCode" : "404"});
                    return;
                }
                res.json(driver);
            }

        });  
    })
    
    /**
     * PATCH call for the driver entity (single).
     * @param {string} firstName - The first name of the new driver
     * @param {string} lastName - The last name of the new driver
     * @param {date} dateOfBirth - The date of birth of the new driver
     * @param {string} licenseType - The license type of the new driver
     * @param {string} username - The username of the new driver
     * @param {string} password - The password of the new driver
     * @param {string} addressLine1 - The address line 1 of the new driver
     * @param {string} addressLine2 - The address line 2 of the new driver
     * @param {string} city - The city of the new driver
     * @param {string} state - The state of the new driver
     * @param {number} zip - The zip code of the new driver
     * @param {number} phoneNumber - The phone number of the new driver
     * @returns {object} A message and the driver updated. (200 Status Code)
     * @throws Mongoose Database Error (500 Status Code)
     */
    .patch(function(req, res){        

        Driver.findById(req.params.driver_id, function(err, driver){
        if(err){
                if (err.name == "CastError")    
            {
                res.status(400).json({"errorCode": "1008", "errorMessage" : util.format("%s is an Invalid ID type for %s resource!", err.value,theResource), "statusCode" : "400"});
                return;
            }
            res.status(500).send(err);
            
        }else{

            if (driver == null)
            {
                res.status(400).json({"errorCode": "1007", "errorMessage" : util.format(" No %s with ID='%s' exists!", theResource, req.params.driver_id), "statusCode" : "400"});
                return;
            }

            var x=0;
    
            for (var key in req.body){x++ }
            if (x==0)
            {
                res.status(400).json({"errorCode": "1001", "errorMessage" : util.format("Empty Request body for %s resource!", theResource), "statusCode" : "400"});
                return;
            }

            var json = req.body;
    
            for (var key in json) {
                if (key.trim() != "firstName" && key.trim() !="lastName" && key.trim() != "emailAddress" && key.trim() != "password" && key.trim() != "addressLine1" &&
                    key.trim() != "addressLine2" && key.trim() != "city" && key.trim() != "state" && key.trim() != "zip" && key.trim() != "phoneNumber" &&
                    key.trim() != "drivingLicense" && key.trim() != "licensedState")
                {
                    res.status(400).json({"errorCode": "1003", "errorMessage" : util.format("Invalid attribute '%s' for the '%s' resource!", key, theResource), "statusCode" : "400"});
                    return;
                }

                if (key.trim() == "password")
                {
                    res.status(400).json({"errorCode": "1016", "errorMessage" : util.format("Not allowed! '%s' of the '%s' resource can't be provided here!", key, theResource), "statusCode" : "400"});
                    return;
                }

                if (json[key] == "" && (key.trim() != "addressLine1" && key.trim() != "addressLine2" && key.trim() != "city" && key.trim() != "state" && key.trim() != "zip"))
                {
                    res.status(400).json({"errorCode": "1004", "errorMessage" : util.format("Missing attribute value! Please provide a value for '%s' of the '%s' resource!", key, theResource), "statusCode" : "400"});
                    return;
                }

                if (json[key] instanceof Array)
                {
                    res.status(400).json({"errorCode": "1005", "errorMessage" : util.format("Duplicate attribute for '%s' of the '%s' resource!", key, theResource), "statusCode" : "400"});
                    return;
                }       

            }



            for(var key in req.body) {
                if(req.body.hasOwnProperty(key)){
                    if(key == 'firstName'){
                        theAttribute = "firstName";
                        if (req.body.firstName.trim().length > 15)
                        {
                                theLength = 15;
                                res.status(400).json({"errorCode": "1013", "errorMessage" : util.format("Invalid length for '%s' of '%s' resource! Length for this attribute should be less than '%d' characters.", theAttribute,theResource,theLength), "statusCode" : "400"});
                                return;
                        }
                        driver.firstName = req.body.firstName;
                    }

                    if(key == 'lastName'){
                        theAttribute = "lastName";
                        if (req.body.lastName.trim().length > 15)
                        {
                                theLength = 15;
                                res.status(400).json({"errorCode": "1013", "errorMessage" : util.format("Invalid length for '%s' of '%s' resource! Length for this attribute should be less than '%d' characters.", theAttribute,theResource,theLength), "statusCode" : "400"});
                                return;
                        }
                        driver.lastName = req.body.lastName;
                    }


                    if(key == 'emailAddress'){
                            theAttribute = "emailAddress";
                            theFormat = "Email address format"
                            if (!isValidEmail(req.body.emailAddress))
                            {
                                    res.status(400).json({"errorCode": "1043", "errorMessage" : util.format("Invalid format for '%s' of '%s' resource! accepted format is: '%s'.", theAttribute,theResource,theFormat), "statusCode" : "400"});
                                    return;
                            }
                            driver.emailAddress = req.body.emailAddress;
                    }

                    if(key == 'phoneNumber'){
                            theAttribute = "phoneNumber";
                            theFormat = "XXX-XX-XXXX";
                            if (!isValidPhone(req.body.phoneNumber))
                            {
                                    res.status(400).json({"errorCode": "1043", "errorMessage" : util.format("Invalid format for '%s' of '%s' resource! accepted format is: '%s'.", theAttribute,theResource,theFormat), "statusCode" : "400"});
                                    return;
                            }
                            driver.phoneNumber = req.body.phoneNumber;
                    }



                    if(key == 'addressLine1'){
                            if (typeof req.body.addressLine1 !== "undefined")
                            {
                                theAttribute = "addressLine1";
                                if (req.body.addressLine1.trim().length > 50)
                                {
                                        theLength = 50;
                                        res.status(400).json({"errorCode": "1013", "errorMessage" : util.format("Invalid length for '%s' of '%s' resource! Length for this attribute should be less than '%d' characters.", theAttribute,theResource,theLength), "statusCode" : "400"});
                                        return;
                                }
                                driver.addressLine1 = req.body.addressLine1;
                            }
                    } 

                    if (key == 'addressLine2')
                    {            
                        if (typeof req.body.addressLine2 !== "undefined")
                        {
                            theAttribute = "addressLine2";
                            if (req.body.addressLine2.trim().length > 50)
                            {
                                    theLength = 50;
                                    res.status(400).json({"errorCode": "1013", "errorMessage" : util.format("Invalid length for '%s' of '%s' resource! Length for this attribute should be less than '%d' characters.", theAttribute,theResource,theLength), "statusCode" : "400"});
                                    return;
                            }
                            driver.addressLine2 = req.body.addressLine2;
                        }
                    }

                    if (key == 'city')
                    { 
                        if (typeof req.body.city !== "undefined")
                        {
                            theAttribute = "city";
                            if (req.body.city.trim().length > 50)
                            {
                                    theLength = 50;
                                    res.status(400).json({"errorCode": "1013", "errorMessage" : util.format("Invalid length for '%s' of '%s' resource! Length for this attribute should be less than '%d' characters.", theAttribute,theResource,theLength), "statusCode" : "400"});
                                    return;
                            }
                            driver.city = req.body.city;
                        }
                    }

                    if (key == 'state')
                    {               
                        if (typeof req.body.state !== "undefined")
                        {
                            theAttribute = "state";
                            if (req.body.state.trim().length != 2)
                            {
                                    theLength = 2;
                                    res.status(400).json({"errorCode": "1033", "errorMessage" : util.format("Invalid length for '%s' of '%s' resource! Length for this attribute should be '%d' characters.", theAttribute,theResource,theLength), "statusCode" : "400"});
                                    return;
                            }
                            driver.state = req.body.state;
                        }
                    }

                    if (key == 'zip')
                    {              
                        if (typeof req.body.zip !== "undefined")
                        {
                            theAttribute = "zip";
                            if (req.body.zip.trim().length != 5)
                            {
                                    theLength = 5;
                                    res.status(400).json({"errorCode": "1033", "errorMessage" : util.format("Invalid length for '%s' of '%s' resource! Length for this attribute should be '%d' characters.", theAttribute,theResource,theLength), "statusCode" : "400"});
                                    return;
                            }
                            driver.zip = req.body.zip;
                        }
                    }

                    if (key == "drivingLicense")
                    {
                        theAttribute = "drivingLicense";
                        if (req.body.drivingLicense.trim().length < 8 || req.body.drivingLicense.trim().length > 16 )
                        {
                                theLength1 = 8;
                                theLength2 = 16;
                                res.status(400).json({"errorCode": "1023", "errorMessage" : util.format("Invalid length range for '%s' of '%s' resource! Length for this attribute should be between '%d' and '%d' characters.", theAttribute,theResource,theLength1,theLength2), "statusCode" : "400"});
                                return;
                        }
                        driver.drivingLicense = req.body.drivingLicense;
                    }

                    if(key == "licensedState")
                    {
                        theAttribute = "LicensedState";
                        if (req.body.licensedState.trim().length != 2)
                        {
                                theLength = 5;

                                res.status(400).json({"errorCode": "1033", "errorMessage" : util.format("Invalid length for '%s' of '%s' resource! Length for this attribute should be '%d' characters.", theAttribute,theResource,theLength), "statusCode" : "400"});
                                return;
                        }
                        driver.licensedState = req.body.licensedState;
                    }
                }
            }

            driver.save(function(err){
                if(err){
                    res.status(500).send(err);
                }else{
                    res.json({"message" : "Driver Updated", "driverUpdated" : driver});
                }
            });
        }
        });
    })

    /**
     * DELETE call for the driver entity (single).
     * @returns {object} A string message. (200 Status Code)
     * @throws Mongoose Database Error (500 Status Code)
     */
    .delete(function(req, res){
        Driver.remove({
            _id : req.params.driver_id
        }, function(err, driver){

            if (typeof req.params.driver_id === "undefined")
            {
                es.status(404).json({"errorCode": "1015", "errorMessage" : util.format(" Missing expected parameter for %s!", theResource), "statusCode" : "404"});
                return;
            }

            if (driver == null)
            {
                res.status(404).json({"errorCode": "1007", "errorMessage" : util.format(" No %s with ID='%s' exists!", theResource, req.params.driver_id), "statusCode" : "404"});
                return;
            }
            if(err){
                if (err.name == "CastError")
                {
                    res.status(404).json({"errorCode": "1008", "errorMessage" : util.format("%s is an Invalid ID type for %s resource!", err.value,theResource), "statusCode" : "404"});
                    return;
                }
                res.status(500).send(err);
            }else{
                res.json({"message" : "Driver Deleted"});
            }

        });
    });

module.exports = router;