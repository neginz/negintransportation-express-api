/** 
 * Example of RESTful API using Express and NodeJS
 * @author Clark Jeria
 * @version 0.0.2
 */

/** BEGIN: Express Server Configuration */
var express    = require('express');
var app        = express();
var bodyParser = require('body-parser');

var CryptoJS=require("crypto-js");
var base64=require("js-base64").Base64;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8080;

var mongoose    = require('mongoose');
//mongoose.connect('mongodb://app_user:password@ds035826.mlab.com:35826/cmu_sv_app');
mongoose.connect('mongodb://negindb22:password@ds033337.mongolab.com:33337/negindb2');

/** END: Express Server Configuration */

/** BEGIN: Express Routes Definition */
var sessions = require('./routes/sessions');
var router = require('./routes/router');
var cars = require('./routes/cars');
var drivers = require('./routes/drivers');
var passengers = require('./routes/passengers');
var paymentAccounts = require('./routes/paymentaccounts');
var rides = require('./routes/rides');


app.use(function(req,res,next)
{
  /** 
   * don't check the token at session call... no Token exists at this point.
   */

  if (req.url != '/api/sessions/')
  {
      var theHeader = JSON.stringify(req.headers);
      theHeader = JSON.parse(theHeader)

      if(typeof theHeader.tokens !== "undefined")
      {
        var token = theHeader.tokens;

        try{
        var theDebase = base64.decode(token);
        var theDecrypt = CryptoJS.AES.decrypt(theDebase,"Secret").toString(CryptoJS.enc.Utf8);
        var theHashString = theDecrypt.substring(theDecrypt.lastIndexOf(":")+1);
        var theClearString = theDecrypt.substring(0,theDecrypt.lastIndexOf(":"));
        var theValidUsername = theClearString.substring(0,theClearString.indexOf(":"));
        var theExpDate = theClearString.substring(0,theClearString.lastIndexOf(":"));
        }
        catch(error)
        {
          res.status(401).json({"errorCode": "995", "errorMessage" : "Missing Token!", "statusCode" : "401"});
          return;
        }

        var currDate = parseInt(Date.now());

        if (currDate >= theExpDate)
        {
            res.status(401).json({"errorCode": "998", "errorMessage" : "Expired Session! Please login again.", "statusCode" : "401"});
            return;
        }

        /** 
         * if someone messes up with the token.
        */
        if (theHashString != CryptoJS.HmacSHA1(theClearString,"APP"))
        {
            res.status(401).json({"errorCode": "999", "errorMessage" : "Authentication Error! Please login again.", "statusCode" : "401"});
            return;
        }
      }
      else
      {
        res.status(401).json({"errorCode": "995", "errorMessage" : "Missing Token!", "statusCode" : "401"});
        return;
      }
  }
  next();
}) 

app.use('/api', sessions);
app.use('/api', cars);
app.use('/api', drivers);
app.use('/api', passengers);
app.use('/api', paymentAccounts);
app.use('/api', rides);
app.use('/api', router);

app.use(function(req, res, next) {
  res.status(404).json({"errorCode": "1012", "errorMessage" : "Invalid Resource Name/Path!", "statusCode" : "404"});  
});
/** END: Express Routes Definition */

/** BEGIN: Express Server Start */
app.listen(port);
console.log('Service running on port ' + port);

module.exports = app;
/** END: Express Server Start */