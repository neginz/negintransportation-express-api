/** 
 * Mongoose Schema for the Entity Ride
 * @author Clark Jeria
 * @version 0.0.3
 */

var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;


var RideSchema   = new Schema({

     passenger: { type: Schema.Types.ObjectId, ref: 'Passenger' },
     driver: { type: Schema.Types.ObjectId, ref: 'Driver' },
     car: { type: Schema.Types.ObjectId, ref: 'Car' },
     rideType: String,
     startPoint:  {long:Number,lat:Number},
     endPoint:  {long:Number,lat:Number},
     requestTime: Number,
     pickupTime: Number,
     dropOffTime: Number,
     status: String,
     fare: Number,
     route: [{long: Number, lat:Number}] 
});

module.exports = mongoose.model('Ride', RideSchema);